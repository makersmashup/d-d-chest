#include <SoftwareSerial.h>
#include "DFRobotDFPlayerMini.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN         4 // On Trinket or Gemma, suggest changing this to 1
#define SWITCH_PIN  8 // D8 pin

// How many NeoPixels are attached to the Arduino?
#define NUM_LEDS 15 // Popular NeoPixel ring size


// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel strip(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);
SoftwareSerial mySoftwareSerial(3, 2); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

#define DELAYVAL 20 // Time (in milliseconds) to pause between pixels
#define NUM_COLORS 7

// This 2D array of colors sets the RGB of various flame colors. 
int magicColors[NUM_COLORS][3]={
  {245,191,10},
  {240,168,0},
  {240,208,0},
  {255,200,0},
  {255,123,0},
  {247,200,0},
  {255,85,0}
};

void setup() {
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);
  randomSeed(analogRead(0));
  pinMode(SWITCH_PIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  setAll(255,255,0);

 if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Software Serial intitialization failure. Unit halted."));
    while(true){
      delay(0); // Code to compatible with ESP8266 watch dog.
    }
  }
  myDFPlayer.volume(0);  //Set volume value. From 0 to 30
  myDFPlayer.play(1);  //Play the first mp3
  myDFPlayer.volume(30);  //Set volume value. From 0 to 30
    
}

void loop() {
  int color=random(NUM_COLORS);
  /**
  Serial.println(color);
  Serial.print(magicColors[color][0]);
  Serial.print("-");
  Serial.print(magicColors[color][1]);
  Serial.print("-");
  Serial.println(magicColors[color][2]);
  **/
  Twinkle(magicColors[color][0], magicColors[color][1], magicColors[color][2], 5, DELAYVAL);
   
   if (digitalRead(SWITCH_PIN) == LOW){
      // If button pushed, turn LED on
      digitalWrite(LED_BUILTIN,HIGH);
      // Serial.println("LOW");
   } else {
      // Otherwise, turn the LED off
      digitalWrite(LED_BUILTIN, LOW);
      // Serial.println("HIGH");
   }
}
void Twinkle(byte red, byte green, byte blue, int Count, int SpeedDelay) {
 
  for (int i=0; i<Count; i++) {
     setPixel(random(NUM_LEDS),red,green,blue);
     showStrip();
     delay(SpeedDelay);
   }
 
  delay(SpeedDelay);
}
void showStrip() {
   // NeoPixel
   strip.show();
}

void setPixel(int Pixel, byte red, byte green, byte blue) {
   // NeoPixel
   strip.setPixelColor(Pixel, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
// implement a notification class,
// its member methods will get called 
//
